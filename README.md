# R Code Snippets

Library of R code snippets that I use on a consistent basis.
These snippets are particularly useful to salmon biologists.